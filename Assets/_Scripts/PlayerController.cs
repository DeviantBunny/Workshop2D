using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public Animator playerAnim;
    public GameObject torch;
    public float moveSpeed;
    public float jumpDistance;
    public bool hasTorch;
    
    private WorkshopInputActions inputActions;
    private InputAction movementInput;
    private Rigidbody2D body;
    private Vector2 moveDirection;
    private bool isJumping;

    private void Awake()
    {
        inputActions = new WorkshopInputActions();
        body = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        moveDirection = new Vector2(movementInput.ReadValue<Vector2>().x, 0); 
        playerAnim.SetBool("isRunning", moveDirection.x != 0);

        if (moveDirection.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

        if (moveDirection.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (body.velocity.y == 0)
        {
            isJumping = false;
            playerAnim.SetBool("isJumping", false);
        }
    }

    public void GainTorch()
    {
        hasTorch = true;
        torch.SetActive(true);
    }

    private void FixedUpdate()
    {
        body.AddForce(moveDirection * moveSpeed);
    }

    private void OnJump(InputAction.CallbackContext obj)
    {
        if (isJumping) return;
        body.AddForce(transform.up * jumpDistance, ForceMode2D.Impulse);
        isJumping = true;
        playerAnim.SetBool("isJumping", true);
    }
    
    private void OnAttack(InputAction.CallbackContext obj)
    {
        //throw new NotImplementedException();
    }

    private void OnEnable()
    {
        movementInput = inputActions.Player.Movement;
        inputActions.Player.Jump.performed += OnJump;
        inputActions.Player.Attack.performed += OnAttack;
        
        movementInput.Enable();
        inputActions.Player.Jump.Enable();
        inputActions.Player.Attack.Enable();
        
    }
    
    private void OnDisable()
    {
        movementInput.Disable();
        inputActions.Player.Jump.Disable();
        inputActions.Player.Attack.Disable();
    }
}
