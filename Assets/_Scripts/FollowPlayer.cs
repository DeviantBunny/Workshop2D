using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lawmaker
{
    public class FollowPlayer : MonoBehaviour
    {
        public Transform player;

        private Vector2 offset;

        private void Start()
        {
            offset = transform.position - player.position;
        }

        private void FixedUpdate()
        {
            var newPosition = (Vector2)player.position + offset;
            transform.position = newPosition;
        }
    }
}