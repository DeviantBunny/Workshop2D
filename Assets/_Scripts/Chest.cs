using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Chest : MonoBehaviour
{
    public UnityEvent onInteract;
    private Animator chestAnim;

    private void Awake()
    {
        chestAnim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        onInteract.Invoke();
        chestAnim.SetBool("playerClose", true);
    }
}
