using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brazier : MonoBehaviour
{
    public List<GameObject> effects;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player.hasTorch)
        {
            foreach (var item in effects)
            {
                item.SetActive(true);
            }
        }

    }
}
