using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour
{
    private WorkshopInputActions inputActions;

    private void Awake()
    {
        inputActions = new WorkshopInputActions();
    }

    private void OnRestart(InputAction.CallbackContext obj)
    {
        Restart();
    }

    public static void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnEnable()
    {
        inputActions.UI.Restart.performed += OnRestart;
        inputActions.UI.Restart.Enable();
    }

    private void OnDisable()
    {
        inputActions.UI.Restart.Disable();
    }
}
